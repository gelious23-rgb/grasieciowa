using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class GameControl : MonoBehaviour
{
    [SerializeField] private List<Card> allCards; 
    [SerializeField] private GameObject card_obj;
   // [SerializeField] private GameObject ActiveCardsPanel; 
  [SerializeField] internal List<Card> ActiveCards; //If you have enough points you can use several cards
  [SerializeField]  internal List<GameObject> CardDeck = new List<GameObject>();
  [SerializeField]internal int CardPoints;
  private Canvas canvasRef;
  [SerializeField] private GameObject layout;   
  private Vector3 pos = new Vector3(412f, 27f, 0f);
  
  void Start()
    {
        //canvasRef = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();
        StartTurn(7);
        CardPoints = 4; 
    }
  private void StartTurn(int CardsInHand)
    {
        for (int i = 0; i < CardsInHand; i++)
        {
            Deck();
        }
    }
    private void Deck() //Spawn a deck of cards to choose 
    {
        //Evaluate which cards to spawn; make sure that at least 1 card costing at least 2 points less than max points fot this turn
            //if player has less than 3 points make sure he gets at least one card costing less than 3 points. 
        
            var New_card = Instantiate(card_obj, layout.transform);
            
                var assign = allCards[Random.Range(0, allCards.Capacity)];
                New_card.GetComponent<RealCard>().OnDeck(assign);
               
                //  CardDeck.Add(New_card.gameObject);
    }
    public void EndTurn() //Takes effect of each card in order of activation 
    {
        foreach (var card in ActiveCards) // for each card
        {
            foreach (var effect in card.cardEffects) //Apply its effects 
            {
                TakeEffect(effect);
            }
        }

        int i = 7 - ActiveCards.Capacity;
        foreach (var card in CardDeck)
        {
            Destroy(card);
            
        }
        ActiveCards.Clear();
        CardDeck.Clear();
        StartTurn(i);
       
    }
    private void TakeEffect(Card.CardEffect effect) //Placeholder for testing 
    {
        var logMessage = effect.abilities switch
        {
            Card.CardEffect.Abilities.ATTACK => "Attacked for {0} damage",
            Card.CardEffect.Abilities.DEFEND => "Defended from {0} damage",
            Card.CardEffect.Abilities.PARRY => "Parried for {0} damage",
            _ => string.Empty
        };
        var effectValue = Random.Range(effect.EffectValues.x,effect.EffectValues.y+1); 
        Debug.Log(string.Format(logMessage, effectValue));
    }
}
