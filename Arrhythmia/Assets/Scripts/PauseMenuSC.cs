using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PauseMenuSC : MonoBehaviour
{
    private GameObject[] Cards;
    [SerializeField] private GameObject Menu;
    [SerializeField] private GameObject SettingsM;
    [SerializeField] private Button SettingsB, LeaveB, QuitB;
    [SerializeField] private GameObject Are_U_Sure;
    [SerializeField] private TextMeshProUGUI A_U_S_txt;
    private bool isPaused;

    private bool IsQit, Isleave;

    private void Start()
    {
        QuitB.onClick.AddListener(OpenQMenu);
        LeaveB.onClick.AddListener(OpenLMenu);
        SettingsB.onClick.AddListener(OpenSMenu);
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Pause()
    {
        Menu.SetActive(true);
        Time.timeScale = 0;
        Cards = GameObject.FindGameObjectsWithTag("Card");// wymieni
        foreach (var card in Cards)
        {
            card.GetComponent<RealCard>().enabled = false;
        }

        isPaused = true;
    }

    public void Resume()
    {
        Menu.SetActive(false);
        Time.timeScale = 1;
        foreach (var card in Cards)
        {
            card.GetComponent<RealCard>().enabled = true;
        }

        isPaused = false;
    }

    public void OpenQMenu()
    {
        Are_U_Sure.SetActive(true);
        A_U_S_txt.text = A_U_S_txt.text.Replace("{%action%}", "quit");
        IsQit = true;
    }

    public void OpenLMenu()
    {
        Are_U_Sure.SetActive(true);
        A_U_S_txt.text = A_U_S_txt.text.Replace("{%action%}", "leave the match");
        Isleave = true; 
    }

    public void OpenSMenu()
    {
        SettingsM.SetActive(true);
        
    }
    public void chooseAction()
    {
        if (IsQit)
        {
            IsQit = false; 
            Quit();
        }
        if (Isleave)
        {
            Isleave = false; 
            Leave();
        }
    }
    private void Quit()
    {
        Application.Quit();
    }
    private void Leave()
    {
       Debug.Log("Leaving");
    }

}
