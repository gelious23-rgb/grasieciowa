using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems; 

[RequireComponent(typeof(CanvasGroup))]public class RealCard : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [SerializeField] private TextMeshProUGUI CardName;
    [SerializeField] private Image CardArt;
    [SerializeField] private TextMeshProUGUI CardDescription;
    [SerializeField] private Image[] CardAbility;
    [SerializeField] private TextMeshProUGUI CardCost; 
    private RectTransform Transform;
    private Canvas thisCanvas;
    private CanvasGroup cGroup;
  [SerializeField]  internal Card Identity;
  private GameControl controller; 
    
    
    private void Awake()
    {
        Transform = GetComponent<RectTransform>();
     var _canvas = GameObject.FindGameObjectWithTag("Canvas");
        thisCanvas = _canvas.GetComponent<Canvas>();
        cGroup = GetComponent<CanvasGroup>(); 
        controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameControl>();
     
    }

    internal void OnDeck(Card card)
    {
        Identity = card;
        gameObject.name = card.name + " (Card)";
        CardCost.text = Identity.cost.ToString();
        CardName.text = Identity.cardName;
        CardArt = Identity.picture;
        CardDescription.text = Identity.description;
        foreach (var image in CardAbility)
        {
          // image.sprite = Identity.cardEffects[CardAbility[]].abilityImage.sprite; 
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (controller.ActiveCards.Contains(this.Identity))
        {
            controller.CardDeck.Remove(gameObject);
            controller.CardPoints += Identity.cost;
            controller.ActiveCards.Remove(this.Identity);
        }
        else if(controller.ActiveCards.Contains(this.Identity) == false & controller.CardPoints- Identity.cost > -1)
        {
            controller.CardDeck.Add(gameObject);
            controller.CardPoints -= Identity.cost;
            controller.ActiveCards.Add(Identity);
        }
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        cGroup.blocksRaycasts = false; 
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        cGroup.blocksRaycasts = true;
            //controller.Drop(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Transform.anchoredPosition += eventData.delta / thisCanvas.scaleFactor;
    }
}
