using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu(fileName = "New Card")]
[System.Serializable]
public class Card : ScriptableObject
{
    /* 
     This is the blueprint of a Card.
     CardEffect is a part that matters.     
     Everything else is just cosmetics  
     */
  [SerializeField] internal string cardName ;
  [SerializeField] internal int cost; //cost in points acquired during Rhythm stage 
  [SerializeField] internal Image picture; //Artwork
  [TextArea] [SerializeField] internal string description; //What the card does 
  [SerializeField] internal List<CardEffect> cardEffects; 
  [System.Serializable]
  public struct CardEffect 
  { 
    public Abilities abilities;
    public Image abilityImage;
      public enum Abilities //Ability list will be expanded
      {
          ATTACK,
          DEFEND,
          PARRY
      };
      
// Effect Values can be randomized
      [SerializeField] internal Vector2Int EffectValues;
      
  }
}
